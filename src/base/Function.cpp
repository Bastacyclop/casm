#include <Function.h>

Function::Function(){
  _head = NULL;
  _end = NULL;
  BB_computed = false;
  BB_pred_succ = false;
  dom_computed = false;
}

Function::~Function(){}

void Function::set_head(Line *head){
  _head = head;
}

void Function::set_end(Line *end){
  _end = end;
}

Line* Function::get_head(){
  return _head;
}

Basic_block* Function::get_firstBB(){
   return _myBB.front();
}

Line* Function::get_end(){
  return _end;
}
void Function::display(){
  cout<<"Begin Function"<<endl;
  Line* element = _head;

  if(element == _end)	
    cout << _head->get_content() <<endl;

  while(element != _end){
    cout << element->get_content() <<endl;
		
    if(element->get_next()==_end){
      cout << element->get_next()->get_content() <<endl;
      break;
    }
    else element = element->get_next();

  }
  cout<<"End Function\n\n"<<endl;
	
}

int Function::size(){
  Line* element = _head;
  int lenght=0;
  while(element != _end)
    {
      lenght++;		
      if (element->get_next()==_end)
	break;
      else
	element = element->get_next();
    }
  return lenght;
}	


void Function::restitution(string const filename){
	
  Line* element = _head;
  ofstream monflux(filename.c_str(), ios::app);

  if(monflux){
    monflux<<"Begin"<<endl;
    if(element == _end)	
      monflux << _head->get_content() <<endl;
    while(element != _end)
      {
	if(element->isInst() || 
	   element->isDirective()) 
	  monflux<<"\t";
	
	monflux << element->get_content() ;
	
	if(element->get_content().compare("nop")) 
	  monflux<<endl;
		
	if(element->get_next()==_end){
	  if(element->get_next()->isInst() || 
	     element->get_next()->isDirective())
	    monflux<<"\t";
	  monflux << element->get_next()->get_content()<<endl;
	  break;
	}
	else element = element->get_next();

      }
    monflux<<"End\n\n"<<endl;
		
  }

  else {
    cout<<"Error cannot open the file"<<endl;
  }

  monflux.close();
}

void Function::comput_label(){
  Line* element = _head;

  if(element == _end && element->isLabel())	
    _list_lab.push_back(getLabel(element));
  while(element != _end)
    {

      if(element->isLabel())	
	_list_lab.push_back(getLabel(element));

      if(element->get_next()==_end){
	if(element->isLabel())	
	  _list_lab.push_back(getLabel(element));
	break;
      }
      else element = element->get_next();

    }

}

int Function::nbr_label(){
  return _list_lab.size();

}

Label* Function::get_label(int index){

  list<Label*>::iterator it;
  it=_list_lab.begin();

  int size=(int) _list_lab.size();
  if(index< size){
    for (int i=0; i<index;i++ ) it++;
    return *it;	
  }
  else cout<<"Error get_label : index is bigger than the size of the list"<<endl; 
	
  return _list_lab.back();
}

Basic_block *Function::find_label_BB(OPLabel* label){
  //Basic_block *BB = new Basic_block();
   int size=(int)_myBB.size();
   string str;
   for(int i=0; i<size; i++){		
      if(get_BB(i)->is_labeled()){
	 
	 str=get_BB(i)->get_head()->get_content();
	 if(!str.compare(0, (str.size()-1),label->get_op())){
	    return get_BB(i);
	 }
      }
  }
  return NULL;
}


/* ajoute nouveau BB � la liste de BB de la fonction en le creant */

void Function::add_BB(Line *debut, Line* fin, Line *br, int index){
   Basic_block *b=new Basic_block();
   b->set_head(debut);
   b->set_end(fin);
   b->set_index(index);
   b->set_branch(br);
   _myBB.push_back(b);
}


// Calcule la liste des blocs de base : il faut d�limiter les BB,
// en parcourant la liste des lignes/instructions � partir de la premiere,
// il faut s'arreter � chaque branchement (et prendre en compte le delayed slot qui appartient
// au meme BB, c'est l'instruction qui suit tout branchement) ou � chaque label 
// (on estime que tout label est utilis� par un saut et donc correspond bien � une entete de BB).

void Function::comput_basic_block(){
  bool verbose = true; 
  cout<< "comput BB" <<endl;
  if (verbose) {
    cout<<"head :"<<_head->get_content()<<endl;
    cout<<"tail :"<<_end->get_content()<<endl;
  }
  if (BB_computed) return; // NE PAS TOUCHER

  Line* beg = get_head();
  int index = 0;

  while (beg && beg->isDirective()) {
      beg = beg->get_next();
  }

  Line* cur = beg;
  bool found_instruction = false;
  while (cur != _end->get_next()) {
      if (cur->isLabel()) {
            if (found_instruction) {
                add_BB(beg, cur->get_prev(), NULL, index);
                found_instruction = false;
                index++;
            }
            beg = cur;
        } else if (cur->isInst()) {
            found_instruction = true;
            Instruction* i = getInst(cur);
            if (i->is_branch()) {
                add_BB(beg, cur->get_next(), cur, index);
                cur = cur->get_next();
                beg = cur->get_next();
                found_instruction = false;
                index++;
            }
        }
        cur = cur->get_next();
  }
   
  if (verbose)
    cout<<"end comput Basic Block"<<endl;
   
  BB_computed = true;
  return;
}

int Function::nbr_BB(){
   return _myBB.size();
}

Basic_block *Function::get_BB(int index){

  list<Basic_block*>::iterator it;
  it=_myBB.begin();
  int size=(int)_myBB.size();

  if(index< size){
    for (int i=0; i<index;i++ ) it++;
    return *it;	
  }
  else 
    return NULL;
}

list<Basic_block*>::iterator Function::bb_list_begin(){
   return _myBB.begin();
}

list<Basic_block*>::iterator Function::bb_list_end(){
   return _myBB.end();
}

/* comput_pred_succ calcule les successeurs et pr�d�cesseur des BB, pour cela il faut commencer par les successeurs */
/* et it�rer sur tous les BB d'une fonction */
/* il faut determiner si un BB a un ou deux successeurs : d�pend de la pr�sence d'un saut pr�sent ou non � la fin */
/* pas de saut ou saut incontionnel ou appel de fonction : 1 successeur (lequel ?)*/
/* branchement conditionnel : 2 successeurs */ 
/* le dernier bloc n'a pas de successeurs - celui qui se termine par jr R31 */
/* les sauts indirects n'ont pas de successeur */

void Function::comput_succ_pred_BB() {
   if (BB_pred_succ) return;
 
    Basic_block* b;
    for (int index = 0; (b = get_BB(index)); index++) {
        Line* brl = b->get_branch();
        if (brl) {
            Instruction* br = getInst(brl);
            if (br->is_call()) {
                b->set_link_succ_pred(get_BB(index + 1));
            } else if (br->is_cond_branch()) {
                // FIXME: target == null outside this BB
                Basic_block* target = find_label_BB(br->get_op_label());
                b->set_link_succ_pred(target);
                b->set_link_succ_pred(get_BB(index + 1));
            } else if (br->is_indirect_branch()) {
                /* can't do shit */
            } else {
                // FIXME: target == null outside this BB
                Basic_block* target = find_label_BB(br->get_op_label());
                b->set_link_succ_pred(target);
            }
        } else {
          if ((index + 1) < _myBB.size()) {
            b->set_link_succ_pred(get_BB(index + 1));
          }
        }
    }

   // ne pas toucher ci-dessous
   BB_pred_succ = true;
   return;
}

void intersect(vector<bool>& a, vector<bool>& b) {
  for (int i = 0; i < a.size(); i++) {
    a[i] = a[i] && b[i];
  }
} 

void onion(vector<bool>& a, vector<bool>& b) {
  for (int i = 0; i < a.size(); i++) {
    a[i] = a[i] || b[i];
  }
}

void difference(vector<bool>& a, vector<bool>& b) {
  for (int i = 0; i < a.size(); i++) {
    a[i] = a[i] && !b[i];
  }
}

void Function::compute_dom(){
  int size = _myBB.size();
  if (dom_computed) return;
  comput_succ_pred_BB();
 
  vector<Basic_block*> stack;
  stack.push_back(*_myBB.begin());
  int offset = (*_myBB.begin())->get_index();
  while (!stack.empty()) {
    Basic_block* bb = stack.back();
    stack.pop_back();

    int index = bb->get_index() - offset;
    vector<bool> d(size, index != 0 && bb->get_nb_pred() != 0);
    if (index != 0) {
      for (int predi = 0; predi < bb->get_nb_pred(); predi++) {
        Basic_block* pred = bb->get_predecessor(predi);
        intersect(d, pred->Domin);
      }
    }
    d[index] = true;

    if (d != bb->Domin) {
      bb->Domin = d;
      Basic_block* s = bb->get_successor1();
      if (s) { stack.push_back(s); }
      s = bb->get_successor2();
      if (s) { stack.push_back(s); }
    }

  }


  // affichage du resultat
  list<Basic_block*>::iterator it, it2;
  Basic_block *current, *bb, *pred;
  it=_myBB.begin();
  
  for (int j=0; j< size; j++){
    current = *it;
    cout << "Dominants pour BB" << current -> get_index() << " : "; 
    for (int i = 0; i< nbr_BB(); i++){
      if (current->Domin[i]) cout << " BB" << i  ;
    }
    cout << endl;
    it++;
  }
  dom_computed = true;
  return;
}

void Function::compute_live_var(){
  list<Basic_block*>::iterator it, it2;
  Basic_block *current, *bb;
  int size= (int) _myBB.size();

  /* A REMPLIR avec algo vu en cours et en TD*/
 /* algorithme it�ratif qui part des blocs sans successeur, ne pas oublier que lorsque l'on sort d'une fonction le registre $2 contient le r�sultat (il est donc vivant), le registre pointeur de pile ($29) est aussi vivant ! */

 vector<Basic_block*> stack;
 it=_myBB.begin();
 for (int i = 0; i < size; i++) {
   (**it).compute_use_def();

   if ((**it).get_nb_succ() == 0) {
     stack.push_back(*it);
     (**it).LiveOut[2] = true;
     (**it).LiveOut[29] = true;
   }

   it++;
 }

 while (!stack.empty()) {
   bb = stack.back();
   stack.pop_back();

   Basic_block* s = bb->get_successor1();
   if (s) { onion(bb->LiveOut, s->LiveIn); }
   s = bb->get_successor2();
   if (s) { onion(bb->LiveOut, s->LiveIn); }

   vector<bool> in(bb->LiveOut);
   difference(in, bb->Def);
   onion(in, bb->Use);
   if (in != bb->LiveIn) {
     bb->LiveIn = in;

      for (int predi = 0; predi < bb->get_nb_pred(); predi++) {
        stack.push_back(bb->get_predecessor(predi));
      }
   }
}


  // Affichage du resultat
  it2=_myBB.begin();
  for (int j=0; j<size; j++){
    bb = *it2;
    cout << "********* bb " << bb->get_index() << "***********" << endl;
    cout << "LIVE_OUT : " ;
    for(int i=0; i<NB_REG; i++){
      if (bb->LiveOut[i]){
	cout << "$"<< i << " "; 
      }
    }
    cout << endl;
    cout << "LIVE_IN :  " ;
    for(int i=0; i<NB_REG; i++){
      if (bb->LiveIn[i]){
	cout << "$"<< i << " "; 
      }}
    cout << endl;
    it2++;
  }
  return;
}
     



/* en implementant la fonction test de la classe BB, permet de tester des choses sur tous les blocs de base d'une fonction par exemple l'affichage de tous les BB d'une fonction ou l'affichage des succ/pred des BBs comme c'est le cas -- voir la classe Basic_block et la m�thode test */

void Function::test(){
  int size=(int)_myBB.size();
   for(int i=0;i<size; i++){
    get_BB(i)->test();
  }
   return;
}
